using Microsoft.AspNetCore.Mvc;
using Moq;
using MySamples.Projects.Training.Api.Controllers;
using MySamples.Projects.Training.Api.Models;
using MySamples.Projects.Training.Api.Services;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests.ControllerTests
{
    [TestFixture]
    public class TrainingControllerTests
    {
        private TrainingController controller;
        private Mock<ITrainingService> service;

        [SetUp]
        public void Setup()
        {
            service = new Mock<ITrainingService>();

            controller = new TrainingController(service.Object);
        }

        [Test]
        public async Task Should_Return_Bad_Request_If_Model_Invalid()
        {
            controller.ModelState.AddModelError("key", "error message");
            var response = await controller.Post(new AddTrainingInputModel());
            
            Assert.IsInstanceOf<BadRequestResult>(response);
        }

        [Test]
        public async Task Should_Return_Success_Response_If_All_Good()
        {
            service
                .Setup(x => x.AddAsync(It.IsAny<AddTrainingInputModel>()))
                .Returns(() => Task.FromResult(new AddTrainingOutputModel
                {
                    Succeeded = true,
                    Message = "Success",
                    Days = 5
                }));

            var response = await controller.Post(new AddTrainingInputModel());

            Assert.IsInstanceOf<OkObjectResult>(response);

            var responseValue = (response as OkObjectResult).Value;

            Assert.IsInstanceOf<AddTrainingOutputModel>(responseValue);

            var responseModel = responseValue as AddTrainingOutputModel;

            Assert.AreEqual(5, responseModel.Days);
            Assert.AreEqual("Success", responseModel.Message);
            Assert.AreEqual(true, responseModel.Succeeded);

        }
    }
}