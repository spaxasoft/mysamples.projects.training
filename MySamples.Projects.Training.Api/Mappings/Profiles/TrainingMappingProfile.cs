﻿using AutoMapper;
using MySamples.Projects.Training.Api.Models;

namespace MySamples.Projects.Training.Api.Mappings.Profiles
{
    public class TrainingMappingProfile : Profile
    {
        public TrainingMappingProfile()
        {
            CreateMap<AddTrainingInputModel, Data.Training>();
        }
    }
}
