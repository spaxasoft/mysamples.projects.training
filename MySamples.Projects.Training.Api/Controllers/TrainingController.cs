﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySamples.Projects.Training.Api.Models;
using MySamples.Projects.Training.Api.Services;

namespace MySamples.Projects.Training.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors(Startup.CorsPolicyName)]
    public class TrainingController : ControllerBase
    {
        private readonly ITrainingService trainingService;

        public TrainingController(ITrainingService trainingService)
        {
            this.trainingService = trainingService;
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody] AddTrainingInputModel model)
        {            
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var response = await trainingService.AddAsync(model);

            return Ok(response);
        }
    }
}
