﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySamples.Projects.Training.Api.Models;
using MySamples.Projects.Training.Api.Services;

namespace MySamples.Projects.Training.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors(Startup.CorsPolicyName)]
    public class PingController : ControllerBase
    {
        public PingController()
        {
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return Ok("Ping");
        }
    }
}
