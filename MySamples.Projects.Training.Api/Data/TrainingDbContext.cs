﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MySamples.Projects.Training.Api.Data
{
    public class TrainingDbContext: DbContext
    {
            private readonly IConfiguration _configuration;
            private readonly string _connectionStringName;

            public TrainingDbContext(IConfiguration configuration, string connectionStringName = "DefaultConnection")
            {
                _configuration = configuration;
                _connectionStringName = connectionStringName;
            }

            public DbSet<Training> Trainings { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString(_connectionStringName));
            }
        }
}
