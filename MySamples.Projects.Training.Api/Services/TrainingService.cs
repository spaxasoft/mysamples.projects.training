﻿using AutoMapper;
using MySamples.Projects.Training.Api.Data;
using MySamples.Projects.Training.Api.Models;
using System.Threading.Tasks;

namespace MySamples.Projects.Training.Api.Services
{
    public class TrainingService : ITrainingService
    {
        private readonly TrainingDbContext dbContext;
        private readonly IMapper mapper;

        public TrainingService(TrainingDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<AddTrainingOutputModel> AddAsync(AddTrainingInputModel model)
        {
            var training = mapper.Map<Data.Training>(model);

            dbContext.Trainings.Add(training);

            await dbContext.SaveChangesAsync();

            return new AddTrainingOutputModel
            {
                Succeeded = true,
                Message = "Training successfully saved",
                Days = (model.EndDate - model.StartDate).TotalDays
            };
        }
    }
}
