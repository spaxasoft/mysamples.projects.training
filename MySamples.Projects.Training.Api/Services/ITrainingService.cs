﻿using MySamples.Projects.Training.Api.Models;
using System.Threading.Tasks;

namespace MySamples.Projects.Training.Api.Services
{
    public interface ITrainingService
    {
        Task<AddTrainingOutputModel> AddAsync(AddTrainingInputModel model);
    }
}
