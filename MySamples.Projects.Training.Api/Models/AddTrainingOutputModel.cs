﻿namespace MySamples.Projects.Training.Api.Models
{
    public class AddTrainingOutputModel
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public double Days { get; set; }
    }
}
