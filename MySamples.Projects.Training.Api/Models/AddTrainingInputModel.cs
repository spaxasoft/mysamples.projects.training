﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MySamples.Projects.Training.Api.Models
{
    public class AddTrainingInputModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
    }
}
