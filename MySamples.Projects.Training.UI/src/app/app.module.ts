import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TrainingAddComponent } from './training/add/training-add.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TrainingService, ITrainingService } from './training/training.services';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
	TrainingAddComponent
  ],
  imports: [
	BrowserModule,
	NgbModule,
	FormsModule,
	ReactiveFormsModule,
	AppRoutingModule,
	HttpClientModule
  ],
  providers: [
	  {provide: ITrainingService, useClass: TrainingService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
