import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainingAddComponent } from './training/add/training-add.component';

const routes: Routes = [
	{
		path: 'training/new',
		component: TrainingAddComponent
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
