export class TrainingAddModel {
	name: string
	startDate: string
	endDate: string

	constructor(init?: Partial<TrainingAddModel>) {
		if (init) {
			Object.assign(this, init)
		}
	}
}

export class TrainingAddResponse {
	succeeded: boolean
	message: string
	days: number

	constructor(init?: Partial<TrainingAddResponse>) {
		if (init) {
			Object.assign(this, init)
		}
	}
}
