import { Component, OnInit } from '@angular/core';
import { TrainingAddModel, TrainingAddResponse } from '../training.models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { dateCompareValidator } from '../../shared/validation/date-compare.validator';
import { ITrainingService } from '../training.services';
import { ValidationMessages } from 'src/app/shared/validation/validation.messages';

@Component({
	selector: 'app-training-add',
	templateUrl: './training-add.component.html',
	styleUrls: ['./training-add.component.scss']
})
export class TrainingAddComponent implements OnInit {

	model: TrainingAddModel
	formGroup: FormGroup
	isWaiting: boolean = false
	isResponseReady: boolean = false
	response: TrainingAddResponse
	validationMessages = ValidationMessages;

	constructor(
		private formBuilder: FormBuilder,
		private trainingService: ITrainingService
	) {
		this.createValidationRules();
		this.model = new TrainingAddModel()
	}

	createValidationRules() {
		this.formGroup = this.formBuilder.group({
			'name': [null, [Validators.required]],
			'startDate': [null, [Validators.required, Validators.minLength(10)]],
			'endDate': [null, [Validators.required, Validators.minLength(10)]],
		},
			{
				validators: [
					dateCompareValidator('startDate', '<', 'endDate')
				]
			})
	}

	ngOnInit() {
	}

	getDate(value) {
		return value ? `${value.year}-${value.month}-${value.day}` : null
	}

	add() {
		if (!this.formGroup.valid){
			return
		}
		
		this.model.name = this.formGroup.value.name
		this.model.startDate = this.getDate(this.formGroup.value.startDate)
		this.model.endDate = this.getDate(this.formGroup.value.endDate)

		this.isWaiting = true
		this.isResponseReady = false

		this.trainingService.add(this.model).subscribe(
			(data: TrainingAddResponse) => {				
				this.response = data
				this.formGroup.reset()
				this.model = new TrainingAddModel()
			},
			(error) => {
				this.response = new TrainingAddResponse({
					succeeded: false,
					message: "An error occured while trying to save the data",
					days: 0
				})
			},
			() => {
				this.isWaiting = false
				this.isResponseReady = true
			}
		)
	}

}
