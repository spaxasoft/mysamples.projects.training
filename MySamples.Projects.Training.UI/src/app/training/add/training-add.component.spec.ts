import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingAddComponent } from './training-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ITrainingService } from '../training.services';

describe('TrainingAddComponent', () => {
  let component: TrainingAddComponent;
  let fixture: ComponentFixture<TrainingAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
		imports: [
			FormsModule,
			ReactiveFormsModule,
			NgbModule
		  ],
		  declarations: [
			TrainingAddComponent
		  ],
		  providers: [
			ITrainingService
		  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have set formgroup', ()=>{
	expect(component.formGroup).toBeTruthy();
  })
});
