import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrainingAddModel } from './training.models';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export abstract class ITrainingService {
	abstract add(model: TrainingAddModel): Observable<Object>;
}

@Injectable()
export class TrainingService implements ITrainingService {

	constructor(private http: HttpClient) {
	}

	add(model: TrainingAddModel) {
		return this.http.post(environment.urls.training.add, model)
	}
}
