export const ValidationMessages = {
	requiredName: "Name is required",
	requiredStartDate: "Start date is required",
	requiredEndDate: "End date is required",
	compareStartAndEndDate: "End date should be after start date"
}