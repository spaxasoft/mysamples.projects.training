import { FormGroup } from '@angular/forms';

export function dateCompareValidator(
	controlNameA: string,
	operator: '<' | '>',
	controlNameB: string
) {
	return (group: FormGroup): { [key: string]: any } => {
		const
			controlA = group.get(controlNameA),
			controlAValue = controlA.value ? `${controlA.value.year}-${controlA.value.month}-${controlA.value.day}` : null,
			controlB = group.get(controlNameB),
			controlBValue = controlB.value ? `${controlB.value.year}-${controlB.value.month}-${controlB.value.day}` : null

		if (!controlAValue || !controlBValue) {
			return null;
		}

		let isValid = false

		switch (operator) {
			case '<':
				isValid = controlAValue < controlBValue;
				break;

			case '>':
				isValid = controlAValue > controlBValue;
				break;
		}

		return isValid ? null: { dateCompare: true }
	}
}
