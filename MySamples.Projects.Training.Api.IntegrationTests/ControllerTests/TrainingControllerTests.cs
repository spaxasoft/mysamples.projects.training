using AutoMapper;
using Microsoft.Extensions.Configuration;
using MySamples.Projects.Training.Api.Controllers;
using MySamples.Projects.Training.Api.Data;
using MySamples.Projects.Training.Api.Mappings.Profiles;
using MySamples.Projects.Training.Api.Models;
using MySamples.Projects.Training.Api.Services;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.ControllerTests
{
    [TestFixture]
    public class TrainingControllerTests
    {
        private IConfiguration configuration;
        private TrainingController controller;
        private ITrainingService service;

        [SetUp]
        public void Setup()
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");

            configuration = configurationBuilder.Build();

            var dbContext = new TrainingDbContext(configuration);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TrainingMappingProfile>();
            });
            var mapper = config.CreateMapper();

            service = new TrainingService(dbContext, mapper);

            controller = new TrainingController(service);
        }

        [Test]
        public async Task Should_Not_Add_To_Db_If_Model_Invalid()
        {
            var model = new AddTrainingInputModel
            {
                Name = "Test_" + Guid.NewGuid(),
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7)
            };

            controller.ModelState.AddModelError("key", "error message");
            var response = await controller.Post(model);

            using (var dbContext = new TrainingDbContext(configuration))
            {
                var training = dbContext.Trainings.FirstOrDefault(x => x.Name == model.Name);

                Assert.IsNull(training);
            }
        }

        [Test]
        public async Task Should_Add_To_Db_If_All_Good()
        {
            var model = new AddTrainingInputModel
            {
                Name = "Test_" + Guid.NewGuid(),
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7)
            };

            var response = await controller.Post(model);

            using (var dbContext = new TrainingDbContext(configuration))
            {
                var training = dbContext.Trainings.FirstOrDefault(x => x.Name == model.Name);

                Assert.IsNotNull(training);
                Assert.AreEqual(model.StartDate, training.StartDate);
                Assert.AreEqual(model.EndDate, training.EndDate);
            }
        }
    }
}